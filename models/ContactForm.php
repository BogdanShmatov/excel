<?php

namespace app\models;

use Yii;
use yii\base\Model;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use PHPMailer\PHPMailer\PHPMailer;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }

    public function saveExcel() {
        // Создаем новую таблицу
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Имя');
        $sheet->setCellValue('B1', 'Email');
        $sheet->setCellValue('A2', $this->name);
        $sheet->setCellValue('B2', $this->email);

        // Сохраняем таблицу в файл Excel
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('data.xlsx');
    }

    public function sendMail() {

        $myemail = "ex@mail.ru";
        $mypassword = "password";
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = "smtp.mail.ru";
        $mail->SMTPAuth = true;
        $mail->Username = $myemail;
        $mail->Password = $mypassword;
        $mail->Port = 587;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

        $mail->setFrom($myemail, "name");
        $mail->addReplyTo($this->email, $this->name);
        $mail->addAddress($myemail);

        $mail->isHTML(true);
        $mail->Subject = $this->subject;
        $mail->Body = "<b>Name:</b> {$this->name}<br><b>Email:</b> {$this->email}<br><br><b>Message:</b><br><br>
    {$this->body}<br><br><b>Date:</b>";

        // Attach the file from the web directory
        $attachmentPath = Yii::getAlias('@webroot/data.xlsx');
        $mail->addAttachment($attachmentPath);

        if ($mail->send()) {
            $_SESSION["mail_success"] = true;
        } else {
            $_SESSION["mail_error"] = true;
        }
    }
}
